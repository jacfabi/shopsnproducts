# shopsnproducts

## Overview
The solution is based on the following tools:

* Django Web Framework
* Django Rest Framework
* Docker containers

Django and DRF are used to implement the RESTful API endpoints. The database system used is sqlite.

The Django project is defined in the *cobiro* directory and the following is the main structure:

* *apps/shops/models.py* defines the Shop class.
* *apps/products/models.py* defines the Product class.
* *apps/shops/api/* implements the restful API for shop objects.
* *apps/products/api/* implements the restful API for product objects.

*docker* and *docker-compose* tools are used to prepare the isolated environments (containers) where the application is going to run.

## How to start and use the solution

First, clone the git repository.

```bash
git clone https://gitlab.com/jacfabi/shopsnproducts.git
```

Before going ahead, please be sure that port 8888 is free.

Then run the following command to prepare the container defined in the docker-compose yml file:

```bash
cd shopsnproducts
sudo docker-compose up --build
```

Wait that the service complete successfully to start, then you are ready to submit use the API.

**Note** the previous commands assume you have installed *docker* and *docker-compose* in your system. If you don't have them you can run the django server with the following command:

```bash
cd shopsnproducts/cobiro
python manage.py runserver 0.0.0.0:8888
```

but this still assumes you have installed the python libraries *Django==2.1* and *djangorestframework==3.7.3* and python version 3.7.

## REST API

The following is the list of REST APIs provided by the solution:

* GET `http://localhost:8888/api/shops/`

Returns a list of all the shops

* GET `http://localhost:8888/api/products/`

Returns a list of all the products

* GET `http://localhost:8888/api/products/?shop=<shop_idx>`

Returns a list of the products of a given shop by shop_idx

* POST `http://localhost:8888/api/shops/`

Create a new shop, e.g. with Curl:
```bash
curl -d '{"name" : "my_shop_name"}' -H "Content-Type:application/json" http://localhost:8888/api/shops/
```

* POST `http://localhost:8888/api/products/`

Create a new product to a given shop by shop_idx, , e.g. with Curl:
```bash
curl -d '{"title": "mytitle", "description": "mydesc", "link": "http://www.example.com", "image_link": "http://www.example.com", "shop": 1}' -H "Content-Type:application/json" http://localhost:8888/api/products/
```

## DRF UI & Admin Page

At the same time, you can use the UI provided by DRF and browse the API at:

`http://localhost:8888/api/shops/`
and
`http://localhost:8888/api/products/`

From the UI you can also create/list shops and products.

In case you want to access the admin page, use user admin and password admin in the following page:

`http://localhost:8888/admin/`

## Optional tasks:

* This readme can be used as basic documentation to understand how to use the API
* The *english_shops.json* data has been successfully looaded into the database. The database is pre populated and already available to be used from the repository. The description field of the products has been left empty to decrease the space of the database and avoid an heavy download.

## What did I focus on?

The main focus was to provide the API endpoints available asap. I have used existing libraries and framework without reinventing the wheel, e.g. I have ensured basic featurs such admin page, pagination, authentication, URL validation...etc.... I gave importance to populating the database with the data sample in order to study the performance in case of significant amount of records loaded.

## What was challenging/easy?

Thanks to Django and DRF, the easy task was to setup the API endpoints. More challenging was to clean *english_shops.json* before loading the data in the database.

## What would I do if I had more time?

I would include the tests suite, which is fundamental for the health of a system. I would think about scalability, integrating a memcache tool to retrieve the data from memory instead of disk. I would access the links to products/images and use the status code of the HTTP responses. If the status code == 200 then I would assume that the link is valid and I would store such information in the database to be able to filter the data based on working/not working links.