from django.db import models

class Shop(models.Model):
    idx = models.AutoField(db_column='Idx', primary_key=True)
    name = models.CharField(db_column='Name', max_length=100)

    def __str__(self):
        return self.name
