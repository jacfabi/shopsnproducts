from django.conf.urls import url

from .views import ShopAPIView

urlpatterns = [
    url(r'^$', ShopAPIView.as_view())
]
