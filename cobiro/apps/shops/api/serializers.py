from rest_framework.serializers import ModelSerializer

from apps.shops.models import Shop

class ShopSerializer(ModelSerializer):
    class Meta:
        model = Shop
        fields = [
            'idx',
            'name'
        ]
