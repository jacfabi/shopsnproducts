from rest_framework.generics import ListAPIView
from rest_framework.mixins import CreateModelMixin
from apps.shops.models import Shop
from .serializers import ShopSerializer

class ShopAPIView(CreateModelMixin, ListAPIView):
    serializer_class = ShopSerializer

    def get_queryset(self):
        return Shop.objects.all()

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
