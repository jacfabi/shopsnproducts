from rest_framework.generics import ListAPIView
from rest_framework.mixins import CreateModelMixin
from apps.products.models import Product
from .serializers import ProductSerializer

class ProductAPIView(CreateModelMixin, ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        qs = Product.objects.all()
        query = self.request.GET.get('shop')
        if query is not None:
            qs = qs.filter(shop= query)
        return qs

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
