from django.conf.urls import url

from .views import ProductAPIView

urlpatterns = [
    url(r'^$', ProductAPIView.as_view())
]
