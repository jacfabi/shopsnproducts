from rest_framework.serializers import ModelSerializer

from apps.products.models import Product

class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'idx',
            'title',
            'description',
            'link',
            'image_link',
            'shop'
        ]
