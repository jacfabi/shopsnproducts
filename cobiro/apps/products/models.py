from django.db import models

from apps.shops.models import Shop

class Product(models.Model):
    idx = models.AutoField(db_column='Idx', primary_key=True)
    title = models.CharField(db_column='Title', max_length=32)
    description = models.CharField(db_column='Status', max_length=1024)
    link = models.URLField(max_length=256)
    image_link = models.URLField(max_length=256)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
