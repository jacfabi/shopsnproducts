#!/usr/bin/env python
import json

with open('english_shops.json') as f:
    data = json.load(f)

shops = {}
for shop in data:
    shop = shop.encode('utf-8').replace(" data feed", "")
    if shop not in shops:
        shops[shop] = len(shops)+1

#prepare json format for shop django model
f = open('shops.json', 'w')
f.write("[")
json_shops = []
for shop in shops:
    json_shops.append("{\"model\": \"shops.shop\", \"idx\":"+str(shops[shop])+", \"fields\":{\"name\": \""+shop+"\"}}")

f.write(",".join(json_shops))
f.write("]")
f.close()

#prepare json format for product django model
f = open('products.json', 'w')
json_products = []
f.write("[")
for shop in data:
    for product in data[shop]:
        title = product['title'].encode('utf-8').replace("\"","").replace("\\", "")
        link = product['link'].encode('utf-8')
        image_link = product['image_link']
        if image_link:
            image_link = image_link.encode('utf-8')
        else:
            image_link = ""
        description = ""#product['description'].encode('utf-8').replace("\"","").replace("\\", "")
        shop_idx = str(shops[shop.encode('utf-8').replace(" data feed", "")])
        if not shop_idx:
            continue
        json_products.append("{\"model\": \"products.product\", \"idx\":"+str(len(json_products)+1)+", \"fields\":{\"title\": \""+title+"\", \"description\": \""+description+"\", \"link\": \""+link+"\", \"image_link\": \""+image_link+"\", \"shop\": "+str(shop_idx)+"}}")
                             
f.write(",".join(json_products))
f.write("]")
